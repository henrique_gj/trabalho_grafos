all:
	gcc -c main.c
	gcc main.o -o exe 

clean:
	rm -rf exe *.o

exec:
	gcc -c main.c
	gcc main.o -o exe
	time ./exe USA-road-d.NY.gr >> saida.txt
	time ./exe USA-road-t.NY.gr >> saida.txt
	time ./exe USA-road-d.BAY.gr >> saida.txt
	time ./exe USA-road-t.BAY.gr >> saida.txt
	time ./exe USA-road-d.COL.gr >> saida.txt
	time ./exe USA-road-t.COL.gr >> saida.txt
	time ./exe USA-road-d.FLA.gr >> saida.txt
	time ./exe USA-road-t.FLA.gr >> saida.txt
	time ./exe USA-road-d.NW.gr >> saida.txt
	time ./exe USA-road-t.NW.gr >> saida.txt