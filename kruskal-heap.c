#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

typedef struct {
	int v1;
	int v2;
	int w;
} Edge;

int *rank, *root;
double peso_arvore = 0, tamanho_arvore = 0;
int nVertices, nArestas, heap_l = -1;
Edge** grafo;

Edge* pull_root() {
    Edge* result = grafo[0];
	Edge* aux;
	int min, i=0;
	
	grafo[0] = grafo[heap_l];
	heap_l--;
	
	while(1) {
		if(2*i+1 <= heap_l) {
			if(2*i+2 <= heap_l) {
				if (grafo[2*i+1]->w < grafo[2*i+2]->w)
					min = 2*i+1;
				else
					min = 2*i+2;
			}
			else
				min = 2*i+1;
		}
		else
			break;
		
		if (grafo[i]->w > grafo[min]->w) {
			aux = grafo[i];
			grafo[i] = grafo[min];
			grafo[min] = aux;
			i = min;
		}
		else
			break;
	}
	
	return result;
}

void insert_edge(Edge* e) {
    heap_l++;
	grafo[heap_l] = e;
	
	int i = heap_l;
	int parent;
	
	Edge* aux;
	
	while(i>0) {
		parent = (i-2+(i%2))/2;
		if (grafo[parent]->w > grafo[i]->w) {
			aux    = grafo[parent];
			grafo[parent] = grafo[i];
			grafo[i] = aux;
			i = parent;
		}
		else
			break;
	}
}

int get_root(int i){
	if(i != root[i]) {
		root[i] = get_root(root[i]);
	}
	
	return root[i];
}

int main(int argc, char* argv[]) {
	clock_t start, end;
    double cpu_time_used;
	
	FILE* entrada = fopen (argv[1], "rb");
	if(!entrada) {
		printf("Erro ao abrir arquivo!\n");
		return 0;
	}
	char buffer[100];
	int i = 0;
	Edge* e;

	int v1, v2, valor;
	while(!feof(entrada)) {
		fgets(buffer, 100, entrada);
		if(buffer[0] == 'p') {
			sscanf(buffer, "p sp %d %d", &nVertices, &nArestas);
			grafo = (Edge**)malloc(nArestas*sizeof(Edge*));
        }
		else if (buffer[0] == 'a') {
			sscanf(buffer, "a %d %d %d", &v1, &v2, &valor);
			e = (Edge*)malloc(sizeof(Edge));
			e->v1 = v1-1;
			e->v2 = v2-1;
			e->w = valor;
			insert_edge(e);
		}
	}
	
    start = clock();
	
	root   = (int*)malloc(nVertices*sizeof(int));
	rank   = (int*)calloc(nVertices,sizeof(int));
	
	for(i=0; i<nVertices; i++) root[i] = i;
	
	for(i=0; tamanho_arvore<nVertices-1; i++) {
		e = pull_root();
		v1 = get_root(e->v1);
		v2 = get_root(e->v2);
		
		if (v1!=v2) {
			if (rank[v1] > rank[v2]) {
			    root[v2]   = v1;
			}
			else {
				root[v1]   = v2;
			}
			
			if (rank[v1] == rank[v2])
				rank[v2]++;
			
			peso_arvore += e->w;
			tamanho_arvore++;
		}
	}
	
	end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	
	printf("\n%s\n", argv[1]);
	printf("Vertices = %d\tArestas = %d\n", nVertices, nArestas);
	printf("Custo = %f\tTamanho = %f\tTempo = %f\n", peso_arvore, tamanho_arvore, cpu_time_used);
}