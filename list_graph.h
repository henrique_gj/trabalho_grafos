#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int edge;
    int weight;
    struct node* next;
} Node;

typedef struct graph {
    struct node** edges;
    int n;
} Graph;

void init(Node** head) {
    *head = NULL;
}

Graph* init_graph(int n) {
    int i;
    Graph *graph = (Graph*)malloc(sizeof(Graph));
    graph->n = n;
    graph->edges = (Node **)malloc((n-1)*sizeof(Node *));
    for (i=0;i<n;i++) init(&graph->edges[i]);
    return graph;
}

void print_list(Node* head) {
    Node * temp;
    for (temp = head; temp; temp = temp->next)
        printf("%d, %d\n", temp->edge, temp->weight);
}

Node* add(Node* node, int edge, int weight) {
    Node* temp = (Node*) malloc(sizeof (Node));
    if (temp == NULL) {
        exit(0); // no memory available
    }
    temp->edge   = edge;
    temp->weight = weight;
    temp->next = node;
    node = temp;
    return node;
}

void add_at(Node* node, int edge, int weight) {
    Node* temp = (Node*) malloc(sizeof (Node));
    if (temp == NULL) {
        exit(EXIT_FAILURE); // no memory available
    }
    temp->edge   = edge;
    temp->weight = weight;
    temp->next = node->next;
    node->next = temp;
}

void remove_node(Node* head) {
    Node* temp = (Node*) malloc(sizeof (Node));
    if (temp == NULL) {
        exit(EXIT_FAILURE); // no memory available
    }
    temp = head->next;
    head->next = head->next->next;
    free(temp);
}

Node * reverse_rec(Node * ptr, Node * previous) {
    Node * temp;
    if (ptr->next == NULL) {
        ptr->next = previous;
        return ptr;
    } else {
        temp = reverse_rec(ptr->next, ptr);
        ptr->next = previous;
        return temp;
    }
}

Node * reverse(Node * node) {
    Node * temp;
    Node * previous = NULL;
    while (node != NULL) {
        temp = node->next;
        node->next = previous;
        previous = node;
        node = temp;
    }
    return previous;
}

Node *free_list(Node *head) {
    Node *tmpPtr = head;
    Node *followPtr;
    while (tmpPtr != NULL) {
        followPtr = tmpPtr;
        tmpPtr = tmpPtr->next;
        free(followPtr);
    }
    return NULL;
}