#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include "list_graph.h"

double prim(Graph* graph) {
	int* parents = (int*)malloc(graph->n*sizeof(int));
 
	int i, parent;
	for(i=0; i<graph->n; i++) {
		parents[i] = -1;
	}
	parents[0] = 0;
	double custoTotal = 0;

	while(1) {
		int min = INT_MAX;
		int min_index;
		for(i = 0; i<graph->n; i++) {
			if(parents[i] != -1) {
				Node* v;
				for(v = graph->edges[i]; v; v = v->next) {
					if(parents[v->edge] == -1 && v->weight < min){
						min = v->weight;
						min_index = v->edge;
						parent = i;
					}
				}
			}
		}
		if(min == INT_MAX) break;
		parents[min_index] = parent;
		custoTotal += min;
	}

	return custoTotal;
}

int main(int argc, char* argv[]) {

	clock_t start, end;
    double cpu_time_used;
	FILE* entrada = fopen (argv[1], "rb");
	if(!entrada) {
		printf("Erro ao abrir arquivo!\n");
		return 0;
	}
	char buffer[100];
	char c;
	int nVertices, nArestas;
    Graph *graph;

	int v1, v2, valor;
	while(!feof(entrada)) {
		fgets(buffer, 100, entrada);
		if(buffer[0] == 'p') {
			sscanf(buffer, "p sp %d %d", &nVertices, &nArestas);
			graph = init_graph(nVertices);
        } else if (buffer[0] == 'a') {
			sscanf(buffer, "a %d %d %d", &v1, &v2, &valor);
			graph->edges[v1-1] = add(graph->edges[v1-1],v2-1,valor);
			graph->edges[v2-1] = add(graph->edges[v2-1],v1-1,valor);
			fgets(buffer, 100, entrada);
		}
	}	
    start = clock();
	
	double custo = prim(graph);
	
	end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

	printf("\n%s\n", argv[1]);
	printf("Vertices = %d\tArestas = %d\n", nVertices, nArestas);
	printf("usto = %f\tTempo = %f\n", custo, cpu_time_used);

	return 0;
}