#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>

typedef struct AdjListNode {
    int dest;
    int weight;
    struct AdjListNode* next;
}AdjListNode;

typedef struct AdjList {
    struct AdjListNode *head;
}AdjList;

typedef struct Graph {
    int V;
    struct AdjList* array;
}Graph;

AdjListNode* newAdjListNode(int dest, int weight) {
    AdjListNode* newNode = (AdjListNode*) malloc(sizeof(AdjListNode));
    newNode->dest = dest;
    newNode->weight = weight;
    newNode->next = NULL;
    return newNode;
}

Graph* createGraph(int V) {
    Graph* graph = (Graph*) malloc(sizeof(Graph));
    graph->V = V;

    graph->array = (AdjList*) malloc(V * sizeof(AdjList));

    for (int i = 0; i < V; ++i)
        graph->array[i].head = NULL;
 
    return graph;
}

void addEdge(Graph* graph, int src, int dest, int weight) {
    AdjListNode* newNode = newAdjListNode(dest, weight);
    newNode->next = graph->array[src].head;
    graph->array[src].head = newNode;

    newNode = newAdjListNode(src, weight);
    newNode->next = graph->array[dest].head;
    graph->array[dest].head = newNode;
}

typedef struct MinHeapNode {
    int  v;
    int key;
}MinHeapNode;

typedef struct MinHeap {
    int size;
    int capacity;
    int *pos;
    MinHeapNode **array;
}MinHeap;

MinHeapNode* newMinHeapNode(int v, int key) {
    MinHeapNode* minHeapNode = (MinHeapNode*)malloc(sizeof(MinHeapNode));
    minHeapNode->v = v;
    minHeapNode->key = key;
    return minHeapNode;
}

MinHeap* createMinHeap(int capacity) {
    MinHeap* minHeap = (MinHeap*)malloc(sizeof(MinHeap));
    minHeap->pos = (int*)malloc(capacity*sizeof(int));
    minHeap->size = 0;
    minHeap->capacity = capacity;
    minHeap->array = (MinHeapNode**)malloc(capacity*sizeof(MinHeapNode*));
    return minHeap;
}

void swapMinHeapNode(MinHeapNode** a, MinHeapNode** b) {
    MinHeapNode* t = *a;
    *a = *b;
    *b = t;
}

void minHeapify(MinHeap* minHeap, int idx) {
    int smallest, left, right;
    smallest = idx;
    left = 2 * idx + 1;
    right = 2 * idx + 2;
 
    if(left < minHeap->size && minHeap->array[left]->key < minHeap->array[smallest]->key )
        smallest = left;
 
    if(right < minHeap->size && minHeap->array[right]->key < minHeap->array[smallest]->key )
        smallest = right;
 
    if(smallest != idx) {
        MinHeapNode *smallestNode = minHeap->array[smallest];
        MinHeapNode *idxNode = minHeap->array[idx];

        minHeap->pos[smallestNode->v] = idx;
        minHeap->pos[idxNode->v] = smallest;

        swapMinHeapNode(&minHeap->array[smallest], &minHeap->array[idx]);
 
        minHeapify(minHeap, smallest);
    }
}

int isEmpty(MinHeap* minHeap) {
    return minHeap->size == 0;
}

MinHeapNode* extractMin(MinHeap* minHeap) {
    if (isEmpty(minHeap))
        return NULL;

    MinHeapNode* root = minHeap->array[0];

    MinHeapNode* lastNode = minHeap->array[minHeap->size - 1];
    minHeap->array[0] = lastNode;

    minHeap->pos[root->v] = minHeap->size-1;
    minHeap->pos[lastNode->v] = 0;

    --minHeap->size;
    minHeapify(minHeap, 0);
 
    return root;
}

void decreaseKey(MinHeap* minHeap, int v, int key) {

    int i = minHeap->pos[v];

    minHeap->array[i]->key = key;

    while (i && minHeap->array[i]->key < minHeap->array[(i - 1) / 2]->key) {
        minHeap->pos[minHeap->array[i]->v] = (i-1)/2;
        minHeap->pos[minHeap->array[(i-1)/2]->v] = i;
        swapMinHeapNode(&minHeap->array[i],  &minHeap->array[(i - 1) / 2]);
 
        i = (i - 1) / 2;
    }
}

int isInMinHeap(MinHeap *minHeap, int v) {
   return (minHeap->pos[v] < minHeap->size);
}

double PrimMST(Graph* graph) {
    int V = graph->V;
    int* parent = (int*)malloc(V*sizeof(int));
    int* key = (int*)malloc(V*sizeof(int));

    MinHeap* minHeap = createMinHeap(V);

    for (int v = 1; v < V; ++v) {
        parent[v] = -1;
        key[v] = INT_MAX;
        minHeap->array[v] = newMinHeapNode(v, key[v]);
        minHeap->pos[v] = v;
    }

    key[0] = 0;
    minHeap->array[0] = newMinHeapNode(0, key[0]);
    minHeap->pos[0] = 0;

    minHeap->size = V;

    while (!isEmpty(minHeap)) {

        MinHeapNode* minHeapNode = extractMin(minHeap);
        int u = minHeapNode->v;

        AdjListNode* pCrawl = graph->array[u].head;
        while (pCrawl != NULL) {
            int v = pCrawl->dest;

            if (isInMinHeap(minHeap, v) && pCrawl->weight < key[v]) {
                key[v] = pCrawl->weight;
                parent[v] = u;
                decreaseKey(minHeap, v, key[v]);
            }
            pCrawl = pCrawl->next;
        }
    }
    double custo = 0;
    for (int v = 0; v < V; v++) {
        custo += key[v];
    } 
    return custo;
}

int main(int argc, char* argv[]) {

   clock_t start, end;
    double cpu_time_used;
   FILE* entrada = fopen (argv[1], "rb");
   if(!entrada) {
      printf("Erro ao abrir arquivo!\n");
      return 0;
   }
   char buffer[100];
   char c;
   int nVertices, nArestas;
   Graph *graph;

   int v1, v2, valor;
   while(!feof(entrada)) {
      fgets(buffer, 100, entrada);
      if(buffer[0] == 'p') {
         sscanf(buffer, "p sp %d %d", &nVertices, &nArestas);
         graph = createGraph(nVertices);
      } else if (buffer[0] == 'a') {
         sscanf(buffer, "a %d %d %d", &v1, &v2, &valor);
         addEdge(graph, v1-1, v2-1, valor);
         addEdge(graph, v2-1, v1-1, valor);
         fgets(buffer, 100, entrada);
      }
   }  
   start = clock();

   double custo = PrimMST(graph);
   
   end = clock();
   cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

   printf("\n%s\n", argv[1]);
   printf("Vertices = %d\tArestas = %d\n", nVertices, nArestas);
   printf("Custo = %f\tTempo = %f\n", custo, cpu_time_used);
   return 0;
}

// https://www.geeksforgeeks.org/gre
