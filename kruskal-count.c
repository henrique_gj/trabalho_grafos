#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <time.h>

#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int v1;
    int v2;
    int valor;
    struct node* next;
} Node;

void init(Node** head) {
    *head = NULL;
}

Node* add(Node* node, int v1, int v2, int valor) {
    Node* temp = (Node*) malloc(sizeof (Node));
    temp->v1   = v1;
    temp->v2   = v2;
    temp->valor = valor;
    temp->next = node;
    return temp;
}

int *parent, *rank;
	
int get_root(int i){
	while(i != parent[i]) {
	  i = parent[i];
	}
	return i;
}

int main(int argc, char* argv[]) {

	clock_t start, end;
    double cpu_time_used;
	FILE* entrada = fopen (argv[1], "rb");
	if(!entrada) {
		printf("Erro ao abrir arquivo!\n");
		return 0;
	}
	
	char buffer[100];
	char c;
	int nVertices, nArestas, i, j=0, maior = 0;
	double peso_arvore = 0, tamanho_arvore = 0;
    Node *edges = NULL;
    Node** count;

	int v1, v2, valor;
	while(!feof(entrada)) {
		fgets(buffer, 100, entrada);
		if(buffer[0] == 'p') {
			sscanf(buffer, "p sp %d %d", &nVertices, &nArestas);
        }
		else if (buffer[0] == 'a') {
			sscanf(buffer, "a %d %d %d", &v1, &v2, &valor);
			edges = add(edges,v1-1,v2-1,valor);
			if (valor>maior)
				maior = valor;
		}
	}	
	
    start = clock();
	
    count = (Node **)malloc((maior+1)*sizeof(Node *));
    for (i=0;i<maior;i++) init(&count[i]);
	
	while(edges != NULL) {
		count[edges->valor] = add(count[edges->valor],edges->v1,edges->v2,edges->valor);
		edges = edges->next;
	}
	
	parent = (int*)malloc(nVertices*sizeof(int));
	rank   = (int*)calloc(nVertices,sizeof(int));
	for(i=0; i<nVertices; i++) parent[i] = i;
	
	for(i=0; tamanho_arvore<nVertices-1; i++) {
		
		if (edges==NULL) {
			do j++; while(count[j]==NULL);
			edges = count[j];
		}
		
		v1 = get_root(edges->v1);
		v2 = get_root(edges->v2);
		
		if (v1!=v2) {
			if (rank[v1] > rank[v2])
				parent[v2] = v1;
			else
				parent[v1] = v2;
			
			if (rank[v1] == rank[v2])
				rank[v2]++;
			
			peso_arvore += edges->valor;
			tamanho_arvore++;
		}
		
		edges = edges->next;
	}
	
	printf("%s\n", argv[1]);
	printf("%d\t%d\n", nVertices, nArestas);
	
	end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	printf("Custo = %f\tTamanho = %f\tTempo = %f\n", peso_arvore, tamanho_arvore, cpu_time_used);
}